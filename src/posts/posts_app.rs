use std::fmt::Debug;
use std::fs;

use serde::Serialize;

use super::model::Post;

pub fn run() {
    println!();
    println!("-----Serialization/Deserialization---------");
    // get paths from command line
    // let input_path = std::env::args().nth(1).unwrap();
    // let output_path = std::env::args().nth(2).unwrap();
    print_separator();
    //deserialize
    println!("Loading Posts..");
    let post_1 = load_post("res/posts/post_1.json");
    let post_2 = load_post("res/posts/post_2.json");
    let posts: Vec<Post> = load_posts("res/posts/posts.json");

    println!("{:?}", post_1);
    println!("{:?}", post_2);
    println!("{:?}", posts);
    println!("Posts loaded!");

    print_separator();
    //serialize
    let output_dir = "res/output/posts";
    println!("Saving single post..");
    let post_path: String = save_post(output_dir, "post_1.json");
    println!("Saved at: {}", post_path);
    print_separator();
    println!("Saving multiple posts..");
    let posts_path: String = save_posts(output_dir, "posts.json");
    println!("Posts saved at: {}", posts_path);
}

fn load_post(path: &str) -> Post {
    let file_content = fs::read_to_string(path).expect("Should have been able to read the file");
    let post: Post = serde_json::from_str(&file_content).expect("JSON was not well formatted");
    post
}

fn load_posts(path: &str) -> Vec<Post> {
    let file_content = fs::read_to_string(path).expect("Should have been able to read the file");
    let posts: Vec<Post> = serde_json::from_str(&file_content).expect("JSON was not well formatted");
    posts
}

fn save_post(dir: &str, file_name: &str) -> String {
    let brand_new_post: Post = Post::new_now("Tomek", "Brand new serialization...");
    serialize_to_file(&brand_new_post, dir, file_name)
}

fn save_posts(dir: &str, file_name: &str) -> String {
    let posts = vec![
        Post::new_now("Tomek", "Brand new serialization 1..."),
        Post::new_now("Tomek", "Brand new serialization 2..."),
        Post::new_now("Tomek", "Brand new serialization 3..."),
    ];
    serialize_to_file(&posts, dir, file_name)
}

fn serialize_to_file<S: Serialize + Debug>(to_serialize: &S, output_dir: &str, file_name: &str) -> String {
    fs::create_dir_all(output_dir).unwrap_or_else(|_| panic!("Cannot create directory {}", output_dir));
    let output_path = [output_dir, "/", file_name].join("");
    fs::write(
        output_path.clone(),
        serde_json::to_string_pretty(&to_serialize).unwrap_or_else(|_| panic!("Failed to serialize: {:?}", to_serialize)),
    ).unwrap_or_else(|_| panic!("Not possible to save at path: {}", output_path));
    output_path
}

fn print_separator() {
    println!();
    println!("____");
    println!();
}

#[cfg(test)]
mod tests {
    use std::fs;
    use std::io::ErrorKind;

    use chrono::{DateTime, NaiveDate, Utc};

    use crate::posts::model::Post;

    #[test]
    fn should_load_post_from_a_file() {
        let result = super::load_post("res/posts/post_1.json");
        let expected = Post::new(
            "Tomek",
            "Post 1",
            NaiveDate::from_ymd_opt(2023, 01, 11).unwrap(),
            DateTime::parse_from_rfc3339("2023-01-11T17:23:11Z").unwrap().with_timezone(&Utc),
        );
        assert_eq!(result, expected)
    }

    #[test]
    #[should_panic(expected = "JSON was not well formatted")]
    fn should_fail_on_invalid_json_format() {
        super::load_post("res/posts/post_invalid_json_format.json");
    }

    #[test]
    #[should_panic(expected = "input contains invalid characters")]
    fn should_fail_on_invalid_date_format() {
        super::load_post("res/posts/post_invalid_date_format.json");
    }

    #[test]
    #[should_panic(expected = "input contains invalid characters")]
    fn should_fail_on_invalid_datetime_format() {
        super::load_post("res/posts/post_invalid_datetime_format.json");
    }

    #[test]
    fn should_load_posts_from_a_file() {
        let result = super::load_posts("res/posts/posts.json");
        let expected = vec![
            Post::new(
                "Tomek",
                "Post 3",
                NaiveDate::from_ymd_opt(2023, 03, 03).unwrap(),
                DateTime::parse_from_rfc3339("2023-03-03T17:23:33Z").unwrap().with_timezone(&Utc),
            ),
            Post::new(
                "Tomek",
                "Post 4",
                NaiveDate::from_ymd_opt(2023, 04, 04).unwrap(),
                DateTime::parse_from_rfc3339("2023-04-04T17:23:44Z").unwrap().with_timezone(&Utc),
            ),
        ];
        assert_eq!(result, expected)
    }

    #[test]
    fn should_save_post_into_a_file() {
        let dir = "res/posts/output";
        let file_name = "test_file.json";
        let binding = [dir, "/", file_name].join("");
        let path = binding.as_str();
        clean_test_file(&path);

        let post_to_save = Post::new(
            "Tomek",
            "Post 1",
            NaiveDate::from_ymd_opt(2023, 01, 11).unwrap(),
            DateTime::parse_from_rfc3339("2023-01-11T17:23:11Z").unwrap().with_timezone(&Utc),
        );

        super::serialize_to_file(&post_to_save, dir, file_name);

        assert!(std::path::Path::new(&path).exists());

        clean_test_file(&path);
    }

    fn clean_test_file(path: &str) {
        let message = match fs::remove_file(path) {
            Ok(()) => "REMOVED".to_string(),
            Err(e)  if e.kind() == ErrorKind::NotFound => "DOES NOT EXIST".to_string(),
            Err(e) => e.to_string(),
        };
        println!("Removing file {} status: {}", path, message)
    }
}