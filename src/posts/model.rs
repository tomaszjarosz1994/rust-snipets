use std::fmt;
use std::fmt::{Debug, Formatter};

use chrono::{DateTime, NaiveDate, Utc};
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct Post {
    creator: String,
    #[serde(alias = "creationDate")]
    creation_date: NaiveDate,
    #[serde(alias = "createdAt")]
    created_at: DateTime<Utc>,
    content: String,
}

impl Debug for Post {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "\
        creator: {},\
        creation_date: {},\
        created_at: {},\
        content: {}", self.creator, self.creation_date, self.created_at, self.content)
    }
}

impl PartialEq for Post {
    fn eq(&self, other: &Self) -> bool {
        self.creator == other.creator &&
            self.content == other.content &&
            self.creation_date == other.creation_date &&
            self.created_at == other.created_at
    }
}

impl Post {
    pub fn new_now(creator: &str, content: &str) -> Self {
        Self { creator: creator.to_string(), creation_date: Utc::now().date_naive(), created_at: Utc::now(), content: content.to_string() }
    }

    pub fn new(creator: &str, content: &str, creation_date: NaiveDate, created_at: DateTime<Utc>) -> Self {
        Self {
            creator: creator.to_string(),
            content: content.to_string(),
            creation_date,
            created_at,
        }
    }
}