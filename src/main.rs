
mod posts;
mod calculator;
mod blockchain;

fn main() {
 posts::run();
 calculator::run();
 // blockchain::run();
}
