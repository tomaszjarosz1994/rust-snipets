use std::ops::{Add, Div, Mul, Sub};

pub fn add<T: Add<T> + Add<Output=T>>(a: T, b: T) -> T {
    a.add(b)
}

pub fn subtract<T: Sub + Sub<Output=T>>(a: T, b: T) -> T {
    a.sub(b)
}


pub fn multiply<T: Mul<T> + Mul<Output=T>>(a: T, b: T) -> T {
    a.mul(b)
}

pub fn divide<T: Div<T> + Div<Output=T>>(a: T, b: T) -> T {
    a.div(b)
}

#[cfg(test)]
mod tests {
    use std::f64::INFINITY;
    use crate::calculator::calculator_app::{add, divide, multiply, subtract};

    #[test]
    fn should_add_ints() {
        let result = add(8, 10);
        assert_eq!(result, 18)
    }

    #[test]
    fn should_add_floats() {
        let result = add(8.2, 10.2);
        assert_eq!(result, 18.4)
    }

    #[test]
    fn should_subtract_ints() {
        let result = subtract(8, 10);
        assert_eq!(result, -2)
    }

    #[test]
    fn should_subtract_floats() {
        let result = subtract(8.2, 10.2);
        assert_eq!(result, -2.0)
    }

    #[test]
    fn should_multiply_ints() {
        let result = multiply(8, 10);
        assert_eq!(result, 80)
    }

    #[test]
    fn should_multiply_floats() {
        let result = multiply(8.2, 10.2);
        assert_eq!(result, 83.63999999999999)
    }

    #[test]
    fn should_divide_ints() {
        let result = divide(8, 10);
        assert_eq!(result, 0)
    }

    #[test]
    fn should_divide_floats() {
        let result = divide(8.2, 10.2);
        assert_eq!(result, 0.803921568627451)
    }

    #[test]
    #[should_panic(expected = "divide by zero")]
    fn should_throw_exception_divide_by_zero_int() {
        divide(8, 0);
    }

    #[test]
    fn should_throw_exception_divide_by_zero_float() {
        let result = divide(8.2, 0.0);
        assert_eq!(result, f64::INFINITY)
    }
}