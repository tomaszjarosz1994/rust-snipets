use crate::calculator::calculator_app::{add, divide, multiply, subtract};

pub fn run(){
    println!();
    println!("-----Calculator---------");
    let a = 36.0;
    let b = 5.0;
    let add_result = add(a,b);
    let subtract_result = subtract(a,b);
    let multiply_result = multiply(a,b);
    let divide_result = divide(a,b);

    println!("Add {} and {} equals {}",a,b,add_result);
    println!("Subtract {} and {} equals {}",a,b,subtract_result);
    println!("Multiply {} and {} equals {}",a,b,multiply_result);
    println!("Divide {} and {} equals {}",a,b,divide_result);

}