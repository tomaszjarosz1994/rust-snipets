use std::time::Duration;

use libp2p::{
    core::upgrade,
    mplex,
    noise::{Keypair, NoiseConfig, X25519Spec},
    swarm::{Swarm, SwarmBuilder},
    tcp::TokioTcpConfig,
    Transport,
};
use log::info;
use tokio::{
    io::{AsyncBufReadExt, BufReader, stdin},
    spawn,
    sync::mpsc,
    time::sleep,
};
use tokio::io::{Lines, Stdin};
use tokio::sync::mpsc::{UnboundedReceiver, UnboundedSender};

use crate::blockchain::blockchain_app::Blockchain;
use crate::blockchain::p2p;
use crate::blockchain::p2p::{AppBehaviour, ChainResponse};

pub async fn create_swarm() -> (Swarm<AppBehaviour>, UnboundedReceiver<bool>, UnboundedReceiver<ChainResponse>, Lines<BufReader<Stdin>>) {
    let (response_sender, response_rcv) = mpsc::unbounded_channel();
    let (init_sender, init_rcv) = mpsc::unbounded_channel();
    let (swarm, stdin) = configure_swarm(response_sender, init_sender).await;
    (swarm, init_rcv, response_rcv, stdin)
}

async fn configure_swarm(response_sender: UnboundedSender<ChainResponse>, init_sender: UnboundedSender<bool>) -> (Swarm<AppBehaviour>, Lines<BufReader<Stdin>>) {
    let auth_keys = Keypair::<X25519Spec>::new()
        .into_authentic(&p2p::KEYS)
        .expect("can create auth keys");

    let transp = TokioTcpConfig::new()
        .upgrade(upgrade::Version::V1)
        .authenticate(NoiseConfig::xx(auth_keys).into_authenticated())
        .multiplex(mplex::MplexConfig::new())
        .boxed();

    let behaviour = AppBehaviour::new(Blockchain::new(), response_sender, init_sender.clone()).await;

    let mut swarm = SwarmBuilder::new(transp, behaviour, *p2p::PEER_ID)
        .executor(Box::new(|fut| {
            spawn(fut);
        }))
        .build();

    let stdin = BufReader::new(stdin()).lines();

    Swarm::listen_on(
        &mut swarm,
        "/ip4/0.0.0.0/tcp/0"
            .parse()
            .expect("can get a local socket"),
    )
        .expect("swarm can be started");

    spawn(async move {
        sleep(Duration::from_secs(1)).await;
        info!("sending init event");
        init_sender.send(true).expect("can send init event");
    });
    (swarm, stdin)
}