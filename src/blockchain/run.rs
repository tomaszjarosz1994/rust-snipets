use libp2p::futures::StreamExt;
use log::{error, info};
use tokio::select;

use crate::blockchain::p2p;
use crate::blockchain::p2p::EventType::{Init, Input, LocalChainResponse};
use crate::blockchain::swarm::create_swarm;

#[tokio::main]
pub async fn run() {
    println!();
    println!("-----Blockchain---------");
    println!("START the application using:");
    println!("RUST_LOG=info cargo run");
    println!("--------------------");
    println!("It’s best to actually start multiple instances of it in different terminal windows.");
    println!("--------------------");
    println!("COMMANDS");
    println!("ls p -> lists all peers");
    println!("ls c -> prints the local blockchain");
    println!("create b $data -> creates a new block with $data as it’s string content");
    println!("--------------------");

    pretty_env_logger::init();

    info!("Peer Id: {}", p2p::PEER_ID.clone());
    let (mut swarm, mut init_rcv, mut response_rcv, mut stdin) = create_swarm().await;

    loop {
        let evt = {
            select! {
                line = stdin.next_line() => Some(p2p::EventType::Input(line.expect("can get line").expect("can read line from stdin"))),
                response = response_rcv.recv() => {
                    Some(p2p::EventType::LocalChainResponse(response.expect("response exists")))
                },
                _init = init_rcv.recv() => {
                    Some(p2p::EventType::Init)
                }
                event = swarm.select_next_some() => {
                    info!("Unhandled Swarm Event: {:?}", event);
                    None
                },
            }
        };

        if let Some(event) = evt {
            match event {
                Init => {
                    let peers = p2p::get_list_peers(&swarm);
                    swarm.behaviour_mut().app.genesis();

                    info!("connected nodes: {}", peers.len());
                    if !peers.is_empty() {
                        let req = p2p::LocalChainRequest {
                            from_peer_id: peers
                                .iter()
                                .last()
                                .expect("at least one peer")
                                .to_string(),
                        };

                        let json = serde_json::to_string(&req).expect("can jsonify request");
                        swarm
                            .behaviour_mut()
                            .floodsub
                            .publish(p2p::CHAIN_TOPIC.clone(), json.as_bytes());
                    }
                }
                LocalChainResponse(resp) => {
                    let json = serde_json::to_string(&resp).expect("can jsonify response");
                    swarm
                        .behaviour_mut()
                        .floodsub
                        .publish(p2p::CHAIN_TOPIC.clone(), json.as_bytes());
                }
                Input(line) => match line.as_str() {
                    "ls p" => p2p::handle_print_peers(&swarm),
                    cmd if cmd.starts_with("ls c") => p2p::handle_print_chain(&swarm),
                    cmd if cmd.starts_with("create b") => p2p::handle_create_block(cmd, &mut swarm),
                    _ => error!("unknown command"),
                },
            }
        }
    }
}
